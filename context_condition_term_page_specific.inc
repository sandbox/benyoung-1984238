<?php

class context_term_page_condition extends context_condition {
  function condition_values() {
    $values = array();

    foreach (taxonomy_get_vocabularies() as $vocab) {
      if (empty($vocab->tags)) {
        foreach (taxonomy_get_tree($vocab->vid) as $term) {
          $values[$term->tid] = check_plain($term->name);
        }
      }
    }

    return $values;
  }

  function condition_form($context) {
    $form = parent::condition_form($context);
    $form['#type'] = 'select';
    $form['#size'] = 12;
    $form['#multiple'] = TRUE;
    $vocabularies = taxonomy_get_vocabularies();
    $options = array();

    foreach ($vocabularies as $vid => $vocabulary) {
      $tree = taxonomy_get_tree($vid);
      if ($tree && (count($tree) > 0)) {
        $options[$vocabulary->name] = array();
        foreach ($tree as $term) {
          $options[$vocabulary->name][$term->tid] = str_repeat('-', $term->depth) . $term->name;
        }
      }
    }

    $form['#options'] = $options;
    return $form;
  }

  function options_form($context) {
    $options = $this->fetch_from_context($context, 'options');

    $form['inheritance'] = array(
      '#type' => 'checkbox',
      '#title' => t('Set this context on the descendants of the selected terms'),
      '#default_value' => empty($options['inheritance']) === false ? $options['inheritance'] : null,
    );

    return $form;
  }

  function execute($tid) {
    if (!$this->condition_used()) {
      return;
    }

    foreach ($this->get_contexts($tid) as $context) {
      $this->condition_met($context, $tid);
    }

    // If $this->values is set, we've met the condition
    if (!empty($this->values)) {
      return;
    }

    $parents = taxonomy_get_parents_all($tid);
    array_shift($parents);

    foreach ($parents as $parent) {
      foreach ($this->get_contexts($parent->tid) as $context) {
        $options = $this->fetch_from_context($context, 'options');

        if ($options && $options['inheritance']) {
          $this->condition_met($context, $parent->tid);
        }
      }

      if (!empty($this->values)) {
        return;
      }
    }
  }
}
